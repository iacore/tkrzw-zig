const std = @import("std");

const sep = std.fs.path.sep_str;

pub fn pkg() std.build.Pkg {
    const cwd = comptime std.fs.path.dirname(@src().file).?;
    return std.build.Pkg{ .name = "tkrzw", .source = .{ .path = cwd ++ sep ++ "tkrzw.zig" } };
}

pub fn link(b: *std.build.Builder, exe: *std.build.LibExeObjStep) !void {
    const tkrzw_dir = comptime std.fs.path.dirname(@src().file).? ++ sep ++ "tkrzw";

    exe.addIncludePath(tkrzw_dir);
    const lib = try library(b);
    lib.setBuildMode(exe.build_mode);
    lib.setTarget(exe.target);
    lib.install();
    exe.linkLibrary(lib);
}

/// simple string-in-string matcher
const StringSeeker = struct {
    pos: usize = 0,
    buffer: []const u8,

    fn seekAfter(this: *@This(), s: []const u8) !void {
        if (s.len == 0) return error.InvalidPattern;
        while (this.pos + s.len <= this.buffer.len) : (this.pos += 1) {
            blk: {
                if (s[s.len - 1] != this.buffer[this.pos + s.len - 1]) break :blk;
                var i: usize = 0;
                while (i < s.len - 1) : (i += 1) {
                    if (s[i] != this.buffer[this.pos + i]) break :blk;
                }
                // found
                this.pos += s.len;
                return;
            }
        }
        return error.PatternNotFound;
    }

    fn seekUntil(this: *@This(), c: u8) ![]const u8 {
        const start = this.pos;
        while (this.pos < this.buffer.len) : (this.pos += 1) {
            if (this.buffer[this.pos] == c) {
                return this.buffer[start..this.pos];
            }
        }
        return error.PatternNotFound;
    }
};

/// extract PACKAGE_VERSION and MYLIBOBJFILES by reading tkrzw/configure
fn read_build_config(allocator: std.mem.Allocator, file: []const u8) !struct {
    version: []const u8,
    mylibobjfiles: []const u8,
} {
    const f = try std.fs.openFileAbsolute(file, .{});
    const buffer = try f.readToEndAlloc(allocator, 200000);
    defer allocator.free(buffer);
    var seeker = StringSeeker{ .buffer = buffer };
    try seeker.seekAfter("PACKAGE_VERSION='");
    const version = try seeker.seekUntil('\'');
    try seeker.seekAfter("MYLIBOBJFILES=\"");
    const mylibobjfiles = try seeker.seekUntil('"');
    return .{
        .version = try allocator.dupe(u8, version),
        .mylibobjfiles = try allocator.dupe(u8, mylibobjfiles),
    };
}

pub fn library(b: *std.build.Builder) !*std.build.LibExeObjStep {
    const tkrzw_dir = comptime std.fs.path.dirname(@src().file).? ++ sep ++ "tkrzw";
    const lib = b.addStaticLibrary("tkrzw", null);
    const config = read_build_config(b.allocator, tkrzw_dir ++ sep ++ "configure") catch |err| {
        std.log.err("Failed to get package version & libobjs", .{});
        return err;
    };
    var iter = std.mem.split(u8, config.mylibobjfiles, " ");
    var cpp_files = std.ArrayList([]const u8).init(b.allocator);
    defer cpp_files.deinit();

    while (iter.next()) |obj| {
        try cpp_files.append(try std.fmt.allocPrint(b.allocator, tkrzw_dir ++ sep ++ "{s}" ++ ".cc", .{obj[0 .. obj.len - 2]}));
    }

    lib.linkLibC();
    lib.linkLibCpp();
    const version = try std.fmt.allocPrint(b.allocator, "\"{s}\"", .{config.version});
    defer b.allocator.free(version);
    lib.defineCMacro("_TKRZW_PKG_VERSION", version);
    lib.defineCMacro("_TKRZW_LIB_VERSION", version);
    lib.addCSourceFiles(cpp_files.items, &.{"-std=c++17"});

    return lib;
}

pub fn build(b: *std.build.Builder) !void {
    const mode = b.standardReleaseOptions();
    const target = b.standardTargetOptions(.{});

    const exe = b.addExecutable("tkrzw-zig-example", "src/example.zig");
    exe.setBuildMode(mode);
    exe.setTarget(target);
    exe.install();

    try link(b, exe);
    exe.addPackage(pkg());

    const run_example_step = b.step("run-example", "Run example");
    run_example_step.dependOn(&exe.run().step);

    const main_tests = b.addTest("src/tests.zig");
    main_tests.setBuildMode(mode);

    try link(b, main_tests);
    main_tests.addPackage(pkg());

    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&main_tests.step);
}
