const c = @cImport({
    @cInclude("../tkrzw/tkrzw_langc.h");
});
const std = @import("std");

pub const INT64MIN = std.math.minInt(i64);

pub fn version() [:0]const u8 {
    return std.mem.span(c.TKRZW_PACKAGE_VERSION);
}

/// free strings allocated by tkrzw
/// e.g. by DBM.get
pub fn free(s: []u8) void {
    c.free(s.ptr);
}

pub const StatusCode = enum(i32) {
    /// Success.
    Success = 0,
    /// Generic error whose cause is unknown.
    Unknown = 1,
    /// Generic error from underlying systems.
    System = 2,
    /// Error that the feature is not implemented.
    NotImplemented = 3,
    /// Error that a precondition is not met.
    Precondition = 4,
    /// Error that a given argument is invalid.
    InvalidArgument = 5,
    /// Error that the operation is canceled.
    Canceled = 6,
    /// Error that a specific resource is not found.
    NotFound = 7,
    /// Error that the operation is not permitted.
    Permission = 8,
    /// Error that the operation is infeasible.
    Infeasible = 9,
    /// Error that a specific resource is duplicated.
    Duplication = 10,
    /// Error that internal data are broken.
    BrokenData = 11,
    /// Error caused by networking failure.
    Network = 12,
    /// Generic error caused by the application logic.
    Application = 13,
};

pub const Status = struct {
    code: StatusCode,
    message: [:0]const u8,
};

pub fn set_last_status(status: Status) void {
    return c.tkrzw_set_last_status(@enumToInt(status.code), status.message.ptr);
}

pub fn get_last_status() Status {
    const status = c.tkrzw_get_last_status();
    return .{
        .code = @intToEnum(StatusCode, status.code),
        .message = std.mem.span(status.message),
    };
}

pub fn get_last_status_code() StatusCode {
    return @intToEnum(StatusCode, c.tkrzw_get_last_status_code());
}

pub fn get_last_status_message() [:0]const u8 {
    return std.mem.span(c.tkrzw_get_last_status_message());
}

pub const Error = error{
    TkrzwUnknown,
    TkrzwSystem,
    TkrzwNotImplemented,
    TkrzwPrecondition,
    TkrzwInvalidArgument,
    TkrzwCanceled,
    TkrzwNotFound,
    TkrzwPermission,
    TkrzwInfeasible,
    TkrzwDuplication,
    TkrzwBrokenData,
    TkrzwNetwork,
    TkrzwApplication,
};

pub fn get_last_error() Error {
    const code = @intToEnum(StatusCode, c.tkrzw_get_last_status_code());
    return switch (code) {
        .Success => unreachable,
        .Unknown => error.TkrzwUnknown,
        .System => error.TkrzwSystem,
        .NotImplemented => error.TkrzwNotImplemented,
        .Precondition => error.TkrzwPrecondition,
        .InvalidArgument => error.TkrzwInvalidArgument,
        .Canceled => error.TkrzwCanceled,
        .NotFound => error.TkrzwNotFound,
        .Permission => error.TkrzwPermission,
        .Infeasible => error.TkrzwInfeasible,
        .Duplication => error.TkrzwDuplication,
        .BrokenData => error.TkrzwBrokenData,
        .Network => error.TkrzwNetwork,
        .Application => error.TkrzwApplication,
    };
}

pub fn get_memory_capacity() i64 {
    return c.tkrzw_get_memory_capacity();
}

pub fn get_memory_usage() i64 {
    return c.tkrzw_get_memory_usage();
}

pub const DBM = struct {
    ptr: *c.TkrzwDBM,

    const This = @This();

    pub fn open(path: [:0]const u8, writable: bool, params: [:0]const u8) Error!This {
        return if (c.tkrzw_dbm_open(path.ptr, writable, params.ptr)) |ptr|
            This{
                .ptr = ptr,
            }
        else
            get_last_error();
    }

    pub fn close(this: This) Error!void {
        return if (!c.tkrzw_dbm_close(this.ptr)) get_last_error();
    }

    pub fn check(this: This, key: []const u8) bool {
        return c.tkrzw_dbm_check(this.ptr, key.ptr, @intCast(i32, key.len));
    }

    /// remember to free the result
    pub fn get(this: This, key: []const u8) ?[]u8 { // todo: change fn sig to error{NOT_FOUND}![]const u8
        var value_len: i32 = 0;
        var maybe_value_ptr = c.tkrzw_dbm_get(this.ptr, key.ptr, @intCast(i32, key.len), &value_len);
        return if (maybe_value_ptr) |value_ptr|
            value_ptr[0..@intCast(usize, value_len)]
        else
            null;
    }

    /// true on success or false on failure (duplicate key + no overwrite).
    pub fn set(this: This, key: []const u8, value: []const u8, overwrite: bool) bool {
        return c.tkrzw_dbm_set(this.ptr, key.ptr, @intCast(i32, key.len), value.ptr, @intCast(i32, value.len), overwrite);
    }

    /// true on success or false on failure.
    pub fn remove(this: This, key: []const u8) bool {
        return c.tkrzw_dbm_remove(this.ptr, key.ptr, @intCast(i32, key.len));
    }

    /// true on success or false on failure.
    pub fn append(this: This, key: []const u8, value: []const u8, delim: []const u8) bool {
        return c.tkrzw_dbm_append(
            this.ptr,
            key.ptr,
            @intCast(i32, key.len),
            value.ptr,
            @intCast(i32, value.len),
            delim.ptr,
            @intCast(i32, delim.len),
        );
    }

    /// true on success or false on failure.
    pub fn compare_exchange(this: This, key: []const u8, expected: []const u8, desired: []const u8) bool {
        return c.tkrzw_dbm_compare_exchange(
            this.ptr,
            key.ptr,
            @intCast(i32, key.len),
            expected.ptr,
            @intCast(i32, expected.len),
            desired.ptr,
            @intCast(i32, desired.len),
        );
    }

    /// @param increment_ The incremental value. If it is TKRZW_INT64MIN, the current value is not changed and a new record is not created.
    /// @param initial The initial value.
    /// @return The current value.
    pub fn increment(this: This, key: []const u8, increment_: ?i64, initial: i64) ?i64 {
        const res = c.tkrzw_dbm_increment(this.ptr, key.ptr, @intCast(i32, key.len), increment_ orelse INT64MIN, initial);
        return if (res == INT64MIN) null else res;
    }

    /// @param overwrite Whether to overwrite the existing record of the new key.
    /// @param copying Whether to retain the record of the old key.
    pub fn rekey(this: This, oldkey: []const u8, newkey: []const u8, overwrite: bool, copying: bool) bool {
        return c.tkrzw_dbm_rekey(this.ptr, oldkey.ptr, @intCast(i32, oldkey.len), newkey.ptr, @intCast(i32, newkey.len), overwrite, copying);
    }

    pub fn make_iterator(this: This) DBMIter {
        return DBMIter{ .ptr = c.tkrzw_dbm_make_iterator(this.ptr) };
    }
};

pub const DBMIter = struct {
    ptr: *c.TkrzwDBMIter,

    const This = @This();

    pub fn deinit(this: This) void {
        c.tkrzw_dbm_iter_free(this.ptr);
    }

    pub fn first(this: This) bool {
        return c.tkrzw_dbm_iter_first(this.ptr);
    }
    pub fn last(this: This) bool {
        return c.tkrzw_dbm_iter_last(this.ptr);
    }
    pub fn next(this: This) bool {
        return c.tkrzw_dbm_iter_next(this.ptr);
    }
    pub fn previous(this: This) bool {
        return c.tkrzw_dbm_iter_previous(this.ptr);
    }

    /// remember to free key&value
    pub fn get(this: This, key: *[]u8, value: *[]u8) bool {
        var key_len: i32 = undefined;
        var value_len: i32 = undefined;
        const res = c.tkrzw_dbm_iter_get(this.ptr, @ptrCast(*[*c]u8, &key.ptr), &key_len, @ptrCast(*[*c]u8, &value.ptr), &value_len);
        if (res) {
            key.len = @intCast(usize, key_len);
            value.len = @intCast(usize, value_len);
        }
        return res;
    }
};
