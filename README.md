# tkrzw-zig

Zig bindings of tkrzw via `tkrzw_langc.h`.

The API is also incomplete. Please contribute!

tkrzw aborts your process when it runs out of memory!

## Usage

First, download this repo with `git clone --recursive xxx.git`.

Then, put in your `build.zig`:
```

const tkrzw = @import("path/to/tkrzw-zig/build.zig");

pub fn build(b: *std.build.Builder) void {
    ...
    tkrzw.link(b, exe); // link library
    exe.addPackage(tkrzw.pkg()); // allow @import("tkrzw") in your code
}
```

## TODO
- more API
    - dbm
        -better params
        -popfirst
        -pushlast
        -various dbm sync
        -various dbm info
    - iter
        - jump
        - remove
        - set
        - add
