const std = @import("std");
const tkrzw = @import("tkrzw");

test "version" {
    std.debug.print("Version: {s}\n", .{tkrzw.version()});
}

test "error & status" {
    tkrzw.set_last_status(.{ .message = "Hello", .code = .Unknown });
    const err = tkrzw.get_last_error();
    try std.testing.expectEqual(err, error.TkrzwUnknown);
}
